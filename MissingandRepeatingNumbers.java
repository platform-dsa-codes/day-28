import java.util.*;

class Solution {
    public int[] findMissingAndRepeatedValues(int[][] grid) {
        int n = grid.length;
        boolean[] seen = new boolean[n * n + 1];

        int repeated = 0;
        int expectedSum = (n * n * (n * n + 1)) / 2;
        int actualSum = 0;

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                int num = grid[i][j];
                if (seen[num]) {
                    repeated = num;
                } else {
                    seen[num] = true;
                }
                actualSum += num;
            }
        }

        int missing = expectedSum - (actualSum - repeated);

        return new int[]{repeated, missing};
    }

    public static void main(String[] args) {
        Solution solution = new Solution();

        int[][] grid1 = {{1, 3}, {2, 2}};
        int[] result1 = solution.findMissingAndRepeatedValues(grid1);
        System.out.println(Arrays.toString(result1)); // Output: [2, 4]

        int[][] grid2 = {{9, 1, 7}, {8, 9, 2}, {3, 4, 6}};
        int[] result2 = solution.findMissingAndRepeatedValues(grid2);
        System.out.println(Arrays.toString(result2)); // Output: [9, 5]
    }
}
